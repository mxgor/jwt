package tests

import (
	"fmt"
	"testing"

	self "gitlab.eclipse.org/eclipse/xfsc/libraries/crypto/jwt"
)

func TestTokenParse(t *testing.T) {
	signed, err, _ := CreateTestJWK(t, true)
	if err != nil {
		fmt.Printf("failed to sign token: %s\n", err)
		return
	}

	token, err := self.Parse(string(signed))

	if err != nil || token == nil {
		t.Error()
	}
}
